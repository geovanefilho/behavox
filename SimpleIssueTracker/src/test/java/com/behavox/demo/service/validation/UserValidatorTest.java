package com.behavox.demo.service.validation;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.validation.BindException;

import com.behavox.demo.model.User;

/**
 * Some tests for the class validation of user
 * @author geovanefilho
 *
 */
public class UserValidatorTest {

	private static User userCorrect;
	private static User userBigName;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		userCorrect = new User("Geovane", "geovanefilho");
		userBigName = new User("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", "admin");
	}

	/**
	 * Test the validation of an user with all the information valid
	 */
	@Test
	public void correctValidation() {
		UserValidator val = new UserValidator();
		BindException errors = new BindException(userCorrect, userCorrect.getClass().getName());
		
		val.validate(userCorrect, errors);
		assertTrue(errors.getAllErrors().isEmpty());
	}
	
	/**
	 * Test the validation of an user when it has a name bigger than permitted
	 */
	@Test
	public void testBigName() {
		UserValidator val = new UserValidator();
		BindException errors = new BindException(userBigName, userBigName.getClass().getName());
		
		val.validate(userBigName, errors);
		assertFalse(errors.getAllErrors().isEmpty());
		assertEquals(1, errors.getAllErrors().size());
		assertTrue(errors.getAllErrors().get(0).getDefaultMessage().contains("must have up to"));
	}

}
