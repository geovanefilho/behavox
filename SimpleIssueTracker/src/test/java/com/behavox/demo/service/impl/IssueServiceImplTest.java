package com.behavox.demo.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.behavox.demo.dao.IssueDAO;
import com.behavox.demo.model.Issue;
import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.model.StatusTransition;
import com.behavox.demo.service.IssueService;
import com.behavox.demo.service.validation.IssueValidator;

/**
 * Some test for the service of issues
 * 
 * @author geovanefilho
 *
 */
public class IssueServiceImplTest {

	@Mock
	private IssueDAO issueDao;
	@Mock
	private IssueValidator validator;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	private static IssueStatus open;
	private static IssueStatus inProgress;
	private static IssueStatus resolved;
	private static IssueStatus reopened;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		open = new IssueStatus(1, "Open", "Open", Boolean.TRUE);
		open.setId(1l);
		inProgress = new IssueStatus(2, "In progress", "In progress", Boolean.FALSE);
		inProgress.setId(2l);
		resolved = new IssueStatus(3, "Resolved", "Resolved", Boolean.FALSE);
		resolved.setId(3l);
		reopened = new IssueStatus(4, "Reopened", "Reopened", Boolean.FALSE);
		reopened.setId(4l);
		
		List<StatusTransition> transitions = new ArrayList<StatusTransition>();
		transitions.add(new StatusTransition(open, inProgress));
		transitions.add(new StatusTransition(open, resolved));
		open.setTransitions(transitions);
	}
	
	/**
	 * Test if method advance is doing right when all of its parameters are right and
	 * its passing the status of code 2 that in the code has to take more time to complete
	 * it's suppose to take 15 seconds
	 */
	@Test
	public void testAdvanceRightSlower() {
		Issue issue = new Issue("Test");
		issue.setStatus(open);
		
		IssueService service = new IssueServiceImpl(issueDao, validator);
		
		when(issueDao.save(issue)).thenReturn(issue);
		Issue changed = service.advance(issue, inProgress);
		
		assertEquals(inProgress.getId(), changed.getStatus().getId());
	}
	
	/**
	 * Test if method advance is doing right when all of its parameters are right and
	 * its passing the status of code 3 that in the code has to take less time to complete
	 * it's suppose to take 5 seconds
	 */
	@Test
	public void testAdvanceRightFast() {
		Issue issue = new Issue("Test");
		issue.setStatus(open);
		
		IssueService service = new IssueServiceImpl(issueDao, validator);
		when(issueDao.save(issue)).thenReturn(issue);
		
		Issue changed = service.advance(issue, resolved);
		assertEquals(resolved.getId(), changed.getStatus().getId());
	}
	
	/**
	 * Test if method advance is doing right when the next status is invalid
	 */
	@Test(expected = ValidationException.class)
	public void testAdvanceFail() {
		Issue issue = new Issue("Teste");
		issue.setStatus(open);
		
		IssueService service = new IssueServiceImpl(issueDao, validator);
		
		service.advance(issue, reopened);
	}
}
