package com.behavox.demo.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.behavox.demo.dao.UserDAO;
import com.behavox.demo.model.User;
import com.behavox.demo.service.GenericService;
import com.behavox.demo.service.validation.UserValidator;

/**
 * Some tests validations for generic class service but using and dao of an user
 * 
 * @author geovanefilho
 *
 */
public class GenericServiceImplTest {

	@Mock
	private UserDAO demo;
	@Mock
	private UserValidator validator;
	
	private static User newUser;
	private static User newUser2;
	private static User userWithId;
	private static User userWithId2;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		newUser = new User("Geovane", "geovanefilho");
		userWithId = new User("Geovane", "geovanefilho");
		userWithId.setId(3l);
		newUser2 = new User("Bryan", "bryan");
		userWithId2 = new User("Bryan", "bryan");
		userWithId2.setId(4l);
	}
	
	/**
	 * test the save method
	 */
	@Test
	public void testSave() {
		GenericService<User> service = new GenericServiceImpl<User>(demo);
		
		when(demo.save(newUser)).thenReturn(userWithId);
		
		User saved = service.save(newUser);
		
		assertEquals(3l, saved.getId().longValue());
	}

	/**
	 * test the saveAll method
	 */
	@Test
	public void testSaveAll() {
		GenericService<User> service = mock(UserServiceImpl.class);
		List<User> users = new ArrayList<User>();
		users.add(newUser);
		users.add(newUser2);
		
		List<User> usersSaved = new ArrayList<User>();
		usersSaved.add(newUser);
		usersSaved.add(newUser2);
		
		doNothing().when(service).saveAll(users);
		
		service.saveAll(users);
		
		assertEquals(usersSaved.get(0).getId(), users.get(0).getId());
	}

}
