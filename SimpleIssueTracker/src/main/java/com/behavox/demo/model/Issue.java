/**
 * 
 */
package com.behavox.demo.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author geovanefilho
 *
 * A model for an Issue
 */
@Entity
public class Issue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=2000, nullable = false)
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar date = Calendar.getInstance();
	
	@ManyToOne(fetch = FetchType.LAZY)
	private User creator;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private IssueStatus status;

	/**
	 * JPA Constructor
	 */
	protected Issue() {
		super();
	}
	
	/**
	 * @param description
	 * @param creator
	 */
	public Issue(String description) {
		super();
		this.description = description;
	}

	/**
	 * @param description
	 * @param creator
	 */
	public Issue(String description, User creator) {
		super();
		this.description = description;
		this.creator = creator;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public IssueStatus getStatus() {
		return status;
	}

	public void setStatus(IssueStatus status) {
		this.status = status;
	}
}
