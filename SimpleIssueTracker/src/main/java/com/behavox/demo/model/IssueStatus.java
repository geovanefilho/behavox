/**
 * 
 */
package com.behavox.demo.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * @author geovanefilho
 *
 * Status for issue
 */
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"code"})})
public class IssueStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=500, nullable = false)
	private String description;
	
	@Column(length=100, nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Integer code;
	
	@Column(nullable = false)
	private Boolean statusDefault = Boolean.FALSE;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "statusFrom")
	private Collection<StatusTransition> transitions;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar inativationDate;

	/**
	 * JPA Constructor
	 */
	protected IssueStatus() {
		super();
	}

	/**
	 * @param code
	 * @param key
	 * @param description
	 * @param name
	 */
	public IssueStatus(Integer code, String description, String name, Boolean statusDefault) {
		super();
		this.description = description;
		this.name = name;
		this.code = code;
		this.statusDefault = statusDefault;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Collection<StatusTransition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Collection<StatusTransition> transitions) {
		this.transitions = transitions;
	}

	public Calendar getInativationDate() {
		return inativationDate;
	}

	public void setInativationDate(Calendar inativationDate) {
		this.inativationDate = inativationDate;
	}

	public Boolean getStatusDefault() {
		return statusDefault;
	}

	public void setStatusDefault(Boolean statusDefault) {
		this.statusDefault = statusDefault;
	}
	
}
