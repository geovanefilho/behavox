package com.behavox.demo.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author geovanefilho
 *
 * Possibles transitions for an Status
 * 
 */
@Entity
@NamedQuery(name="StatusTransition.findAll", query="SELECT st FROM StatusTransition st WHERE st.inativationDate IS NULL ORDER BY st.statusTo.name")
public class StatusTransition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private IssueStatus statusFrom;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private IssueStatus statusTo;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar inativationDate;

	/**
	 * 
	 */
	protected StatusTransition() {
		super();
	}

	/**
	 * @param statusFrom
	 * @param statusTo
	 */
	public StatusTransition(IssueStatus statusFrom, IssueStatus statusTo) {
		super();
		this.statusFrom = statusFrom;
		this.statusTo = statusTo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IssueStatus getStatusFrom() {
		return statusFrom;
	}

	public void setStatusFrom(IssueStatus statusFrom) {
		this.statusFrom = statusFrom;
	}

	public IssueStatus getStatusTo() {
		return statusTo;
	}

	public void setStatusTo(IssueStatus statusTo) {
		this.statusTo = statusTo;
	}

	public Calendar getInativationDate() {
		return inativationDate;
	}

	public void setInativationDate(Calendar inativationDate) {
		this.inativationDate = inativationDate;
	}
	
}
