/**
 * 
 */
package com.behavox.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behavox.demo.model.StatusTransition;

/**
 * @author geovanefilho
 *
 * Data access object for transitions of statuses
 */
public interface StatusTransitionDAO extends JpaRepository<StatusTransition, Long> {
}
