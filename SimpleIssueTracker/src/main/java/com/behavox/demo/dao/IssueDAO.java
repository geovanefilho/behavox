/**
 * 
 */
package com.behavox.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behavox.demo.model.Issue;

/**
 * @author geovanefilho
 *
 * Data access object for Issues
 */
public interface IssueDAO extends JpaRepository<Issue, Long> {
	
}
