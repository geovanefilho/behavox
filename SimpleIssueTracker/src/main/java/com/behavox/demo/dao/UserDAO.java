/**
 * 
 */
package com.behavox.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behavox.demo.model.User;

/**
 * @author geovanefilho
 *
 * Data access object for User
 */
public interface UserDAO extends JpaRepository<User, Long> {

}
