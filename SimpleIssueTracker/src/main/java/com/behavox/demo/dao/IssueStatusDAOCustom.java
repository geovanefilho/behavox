/**
 * 
 */
package com.behavox.demo.dao;

import com.behavox.demo.model.IssueStatus;

/**
 * @author geovanefilho
 *
 * Data access object for statuses of an issue
 */
public interface IssueStatusDAOCustom {

	/**
	 * Find a status by his code
	 * 
	 * @param code
	 * @return
	 */
	public IssueStatus findByCode(Integer code);

	/**
	 * Get the status default
	 * 
	 * @return
	 */
	public IssueStatus getDefault();
}
