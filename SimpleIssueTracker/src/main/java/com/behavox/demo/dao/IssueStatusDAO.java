/**
 * 
 */
package com.behavox.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behavox.demo.model.IssueStatus;

/**
 * @author geovanefilho
 *
 * Data access object for statuses of an issue
 */
public interface IssueStatusDAO extends JpaRepository<IssueStatus, Long>, IssueStatusDAOCustom {
}
