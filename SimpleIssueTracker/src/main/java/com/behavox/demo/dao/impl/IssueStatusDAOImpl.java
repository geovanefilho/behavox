/**
 * 
 */
package com.behavox.demo.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.behavox.demo.dao.IssueStatusDAOCustom;
import com.behavox.demo.model.IssueStatus;

/**
 * @author geovanefilho
 * 
 * Implementation for an status of issue data access
 * 
 */
@Repository
@Transactional(readOnly = true)
public class IssueStatusDAOImpl implements IssueStatusDAOCustom {

	@PersistenceContext
    EntityManager entityManager;
	
	/**
	 * {@inheritDoc}
	 */
	public IssueStatus findByCode(Integer code) {
		TypedQuery<IssueStatus> query = this.entityManager.createQuery("from IssueStatus st WHERE st.code = :code", IssueStatus.class);
		query.setParameter("code", code);
		try {
			return query.getSingleResult();
		} catch(NoResultException nre) {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public IssueStatus getDefault() {
		TypedQuery<IssueStatus> query = this.entityManager.createQuery("from IssueStatus st WHERE st.statusDefault = :default", IssueStatus.class);
		query.setParameter("default", Boolean.TRUE);
		
		try {
			return query.getSingleResult();
		} catch(NoResultException nre) {
			return null;
		}
	}
}
