package com.behavox.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behavox.demo.service.GenericService;

/**
 * Generic controller
 * 
 * @author geovanefilho
 *
 * @param <T>
 */
public abstract class GenericController<T> {

	protected GenericService<T> genericService;

	public GenericController(GenericService<T> genericService) {
		this.genericService = genericService;
	}

	public GenericController() {}

	/**
	 * Get list of entities
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<T> getList() {
		return this.genericService.findAll();
	}

	/**
	 * Get entity saved
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public T getObject(@PathVariable("id") Long id) {
		return this.genericService.find(id);
	}

	/**
	 * Save the entity
	 * 
	 * @param object
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseBody
	public T save(@RequestBody T object) {
		return this.genericService.save(object);
	}
	
	/**
	 * Save list of entity
	 * 
	 * @param object
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/saveList")
	@ResponseBody
	public void saveList(@RequestBody List<T> object) {
		this.genericService.saveAll(object);
	}	

	/**
	 * Update an entity
	 * 
	 * @param object
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/update")
	@ResponseBody
	public T update(@RequestBody T object) {
		return this.genericService.merge(object);
	}
}
