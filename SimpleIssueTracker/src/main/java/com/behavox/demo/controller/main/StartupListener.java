package com.behavox.demo.controller.main;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.model.StatusTransition;
import com.behavox.demo.service.IssueStatusService;

/**
 * Class that execute in the application startup to initialize the data base with the status of the test
 * 
 * @author geovanefilho
 *
 */
@Component
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private IssueStatusService issueStatusService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		initializeBase();
	}
	
	/**
	 * Insert status at the database if it's empty
	 */
	private void initializeBase() {
		final Collection<IssueStatus> statuses = this.issueStatusService.findAll();
		if (statuses == null || statuses.isEmpty()) {
			IssueStatus open = new IssueStatus(1, "When an issue is created and no other action was made for it yet.",
					"Open", Boolean.TRUE);
			statuses.add(open);
			IssueStatus inProgress = new IssueStatus(2, "When an issue is captured and it's fixing.", "In progress",
					Boolean.FALSE);
			statuses.add(inProgress);
			IssueStatus resolved = new IssueStatus(3, "When an issue is resolved.", "Resolved", Boolean.FALSE);
			statuses.add(resolved);
			IssueStatus reopened = new IssueStatus(4, "When an issue was resolved/closed but it had to be open again.",
					"Reopened", Boolean.FALSE);
			statuses.add(reopened);
			IssueStatus closed = new IssueStatus(5, "When an resolved issue pass for a validation and it is fixed.",
					"Closed", Boolean.FALSE);
			statuses.add(closed);

			this.issueStatusService.saveAll(statuses);

			open.setTransitions(new ArrayList<StatusTransition>());
			open.getTransitions().add(new StatusTransition(open, inProgress));
			open.getTransitions().add(new StatusTransition(open, resolved));
			open.getTransitions().add(new StatusTransition(open, closed));
			this.issueStatusService.merge(open);

			inProgress.setTransitions(new ArrayList<StatusTransition>());
			inProgress.getTransitions().add(new StatusTransition(inProgress, resolved));
			inProgress.getTransitions().add(new StatusTransition(inProgress, closed));
			inProgress.getTransitions().add(new StatusTransition(inProgress, open));
			this.issueStatusService.merge(inProgress);

			resolved.setTransitions(new ArrayList<StatusTransition>());
			resolved.getTransitions().add(new StatusTransition(resolved, reopened));
			resolved.getTransitions().add(new StatusTransition(resolved, closed));
			this.issueStatusService.merge(resolved);

			reopened.setTransitions(new ArrayList<StatusTransition>());
			reopened.getTransitions().add(new StatusTransition(reopened, inProgress));
			reopened.getTransitions().add(new StatusTransition(reopened, resolved));
			reopened.getTransitions().add(new StatusTransition(reopened, closed));
			this.issueStatusService.merge(reopened);

			closed.setTransitions(new ArrayList<StatusTransition>());
			closed.getTransitions().add(new StatusTransition(closed, reopened));
			this.issueStatusService.merge(closed);
		}
	}

}
