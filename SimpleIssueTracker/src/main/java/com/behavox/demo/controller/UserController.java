/**
 * 
 */
package com.behavox.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behavox.demo.model.User;
import com.behavox.demo.service.UserService;

/**
 * @author geovanefilho
 * 
 * Controller with rest access to User services
 *
 */
@Controller
@RequestMapping("/user")
public class UserController extends GenericController<User> {

	@Autowired
	private UserService userService;

	@Autowired
	public UserController(UserService userService ) {
		super(userService);
		this.userService = userService;
	}
}