/**
 * 
 */
package com.behavox.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.service.IssueStatusService;

/**
 * @author geovanefilho
 * 
 * Controller with rest access to service of status of issue
 *
 */
@Controller
@RequestMapping("/issueStatus")
public class IssueStatusController extends GenericController<IssueStatus> {
	
	@Autowired
	private IssueStatusService issueStatusService;

	@Autowired
	public IssueStatusController(IssueStatusService issueStatusService) {
		super(issueStatusService);
		this.issueStatusService = issueStatusService;
	}
	
	/**
	 * Request to add a new transition in a status
	 * 
	 * @param idFrom
	 * @param idTo
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/addTransiction")
	public void addTransiction(@RequestParam("idFrom") Long idFrom, @RequestParam("idTo") Long idTo) {
		this.issueStatusService.addNewTransition(idFrom, idTo);
	}
	
}