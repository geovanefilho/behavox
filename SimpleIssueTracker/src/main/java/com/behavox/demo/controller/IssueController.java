/**
 * 
 */
package com.behavox.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.behavox.demo.model.Issue;
import com.behavox.demo.service.IssueService;

/**
 * @author geovanefilho
 * 
 * Controller with rest access to issue's services
 *
 */
@Controller
@RequestMapping("/issue")
public class IssueController extends GenericController<Issue> {
	
	@Autowired
	private IssueService issueService;

	@Autowired
	public IssueController(IssueService issueService ) {
		super(issueService);
		this.issueService = issueService;
	}
	
	/**
	 * Advance an issue for a next step
	 * 
	 * @param idIssue
	 * @param nextStatusId
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/advance")
	public Issue advance(@RequestParam("idIssue") Long idIssue, @RequestParam("nextStatusId") Long nextStatusId) {
		return this.issueService.advance(idIssue, nextStatusId);
	}
	
}