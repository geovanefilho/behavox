/**
 * 
 */
package com.behavox.demo.service;

import org.springframework.beans.factory.annotation.Qualifier;

import com.behavox.demo.model.Issue;
import com.behavox.demo.model.IssueStatus;

/**
 * @author geovanefilho
 *
 *	Services available for issues
 */
@Qualifier(value = "issueService")
public interface IssueService extends GenericService<Issue> {

	/**
	 * Advance an issue for a next step
	 * 
	 * @param idIssue Issue to move forward
	 * @param nextStatusId Next status to move
	 * 
	 * @return Issue with the status changed
	 */
	public Issue advance(Long idIssue, Long nextStatusId);

	/**
	 * Advance an issue for a next step
	 * 
	 * @param idIssue Issue to move forward
	 * @param nextStatusId Next status to move
	 * 
	 * @return Issue with the status changed
	 */
	public Issue advance(Issue issue, IssueStatus next);
}
