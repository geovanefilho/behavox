package com.behavox.demo.service.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.behavox.demo.model.Issue;

/**
 * Class responsible for validate an issue
 * 
 * @author geovanefilho
 *
 */
@Component("issueValidator")
public class IssueValidator implements Validator {

	private int MAX_DESCRIPTION_LENGTH = 1500; //it should use internationalization instead of plain text
	private String DESCRIPTION_LENGTH_VALIDATION = "must have up to " + MAX_DESCRIPTION_LENGTH + " caracters.";
	
    @Override
	public boolean supports(Class<?> clazz) {
		return Issue.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
        
        ValidationUtils.rejectIfEmpty(errors, "description", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD); 
        ValidationUtils.rejectIfEmpty(errors, "status", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD);
                   
        Issue issue = (Issue) target;
        
        if (issue.getDescription() != null && issue.getDescription().length() > MAX_DESCRIPTION_LENGTH) {
        	errors.rejectValue("description", UtilValidator.FIELD_VALIDATION_CODE, DESCRIPTION_LENGTH_VALIDATION);
        }
	}
}

