package com.behavox.demo.service.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.behavox.demo.model.StatusTransition;

/**
 * Class responsible for validate a transition
 * 
 * @author geovanefilho
 *
 */
@Component("statusTransitionValidator")
public class StatusTransitionValidator implements Validator {
	
	/**
     * {@inheritDoc}
     */
    @Override
	public boolean supports(Class<?> clazz) {
		return StatusTransition.class.isAssignableFrom(clazz);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void validate(Object target, Errors errors) {
        
        ValidationUtils.rejectIfEmpty(errors, "statusFrom", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD); 
        ValidationUtils.rejectIfEmpty(errors, "statusTo", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD);
                   
	}
}

