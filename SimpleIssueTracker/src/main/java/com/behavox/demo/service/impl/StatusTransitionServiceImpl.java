/**
 * 
 */
package com.behavox.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import com.behavox.demo.dao.StatusTransitionDAO;
import com.behavox.demo.model.StatusTransition;
import com.behavox.demo.service.StatusTransitionService;

/**
 * @author geovanefilho
 *
 *	Implementation for the services of issues
 */
@Service(value="statusTransitionService")
public class StatusTransitionServiceImpl extends GenericServiceImpl<StatusTransition> implements StatusTransitionService {
	
	@Autowired
	private StatusTransitionDAO statusTransitionDao;
	
	@Autowired
	public StatusTransitionServiceImpl(StatusTransitionDAO statusTransitionDao, @Qualifier("statusTransitionValidator") Validator validator) {
    	this.genericDao = statusTransitionDao;
    	this.statusTransitionDao = statusTransitionDao;
    	
    	this.validator = validator;
	}

}
