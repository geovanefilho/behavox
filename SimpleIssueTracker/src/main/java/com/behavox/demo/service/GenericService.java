/**
 * 
 */
package com.behavox.demo.service;

import java.util.Collection;
import java.util.List;

/**
 * @author geovanefilho
 *
 * Generic service with basics methods
 * 
 */
public interface GenericService<T> {

	/**
	 * Find the register with his id
	 * 
	 * @param id
	 * @return
	 */
	public T find(Long id);
	
	/**
	 * Responsible method for save an information
	 * 
	 * @param model
	 * @return
	 */
	public T save(T model);
	
	/**
	 * Responsible method for update informations
	 * 
	 * @param model
	 */
	public T merge(T model);
	
	/**
	 * Save all
	 * 
	 * @param models
	 */
	public void saveAll(Collection<T> models);
	
	/**
	 * Get all saved registers
	 * 
	 * @return
	 */
	public List<T> findAll();
}
