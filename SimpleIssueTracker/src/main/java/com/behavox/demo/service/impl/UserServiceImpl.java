/**
 * 
 */
package com.behavox.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import com.behavox.demo.dao.UserDAO;
import com.behavox.demo.model.User;
import com.behavox.demo.service.UserService;

/**
 * @author geovanefilho
 *
 *	Implementation for the services of user
 */
@Service(value="userService")
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {
	
	@Autowired
	private UserDAO userDao;
	
	@Autowired
	public UserServiceImpl(UserDAO userDao, @Qualifier("userValidator") Validator validator) {
    	this.genericDao = userDao;
    	this.userDao = userDao;
    	
    	this.validator = validator;
	}

}
