/**
 * 
 */
package com.behavox.demo.service;

import org.springframework.beans.factory.annotation.Qualifier;

import com.behavox.demo.model.StatusTransition;

/**
 * @author geovanefilho
 *
 *	Services available for statuses transition
 */
@Qualifier(value = "statusTransitionService")
public interface StatusTransitionService extends GenericService<StatusTransition> {
}
