package com.behavox.demo.service.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.behavox.demo.dao.IssueStatusDAO;
import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.model.StatusTransition;

/**
 * Class responsible for validate an issue status
 * 
 * @author geovanefilho
 *
 */
@Component("issueStatusValidator")
public class IssueStatusValidator implements Validator {
	
	private String SAME_CODE = "is already in use."; //it should use internationalization instead of plain text
	private int MAX_NAME_LENGTH = 50;
	private String NAME_LENGTH_VALIDATION = "must have up to " + MAX_NAME_LENGTH + " caracters.";
	private int MAX_DESCRIPTION_LENGTH = 250;
	private String DESCRIPTION_LENGTH_VALIDATION = "must have up to " + MAX_DESCRIPTION_LENGTH + " caracters.";
	private String DIFFERENT_FROM_ORIGIN = "must be different from the origin status.";

	@Autowired
	private IssueStatusDAO issueStatusDao;
	
	/**
     * {@inheritDoc}
     */
    @Override
	public boolean supports(Class<?> clazz) {
		return IssueStatus.class.isAssignableFrom(clazz);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void validate(Object target, Errors errors) {
         
        ValidationUtils.rejectIfEmpty(errors, "description", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD);
        ValidationUtils.rejectIfEmpty(errors, "name", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD); 
        ValidationUtils.rejectIfEmpty(errors, "code", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD);
        
        IssueStatus status = (IssueStatus) target;
        if (status.getCode() != null) {
        	IssueStatus saved = issueStatusDao.findByCode(status.getCode());
        	//if object saved exists and id is different from the one that i'm validating
        	if (saved != null && (status.getId() == null || !status.getId().equals(saved.getId()))) {
        		errors.rejectValue("code", UtilValidator.FIELD_VALIDATION_CODE, SAME_CODE);
        	}
        }
        
        if (status.getName() != null && status.getName().length() > MAX_NAME_LENGTH) {
        	errors.rejectValue("name", UtilValidator.FIELD_VALIDATION_CODE, NAME_LENGTH_VALIDATION);
        }
        
        if (status.getDescription() != null && status.getDescription().length() > MAX_DESCRIPTION_LENGTH) {
        	errors.rejectValue("description", UtilValidator.FIELD_VALIDATION_CODE, DESCRIPTION_LENGTH_VALIDATION);
        }
        
        if (status.getTransitions() != null) {
        	for (StatusTransition transition : status.getTransitions()) {
        		IssueStatus to = transition.getStatusTo(); 
        		if (to.getCode() != null && to.getCode().equals(status.getCode())) {
        			errors.rejectValue("transitions", UtilValidator.FIELD_VALIDATION_CODE, DIFFERENT_FROM_ORIGIN);
        		}
        	}
        }
                   
	}
}

