/**
 * 
 */
package com.behavox.demo.service;

import org.springframework.beans.factory.annotation.Qualifier;

import com.behavox.demo.model.User;

/**
 * @author geovanefilho
 *
 *	Services available for user
 */
@Qualifier(value = "userService")
public interface UserService extends GenericService<User> {
}
