/**
 * 
 */
package com.behavox.demo.service.impl;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.validation.ValidationException;

import com.behavox.demo.model.Issue;
import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.model.StatusTransition;

/**
 * 
 * Class responsible to execute the operation provided due the changing of an status
 * 
 * @author geovanefilho
 *
 */
public class ChangeStatusIssue implements Callable<Issue> {

	private Issue issue;
	private IssueStatus nextStatus;
	
	public ChangeStatusIssue(Issue issue, IssueStatus nextStatus) {
		super();
		this.issue = issue;
		this.nextStatus = nextStatus;
	}
	
	public Issue call() throws Exception {
		if (validTransition(this.issue.getStatus(), nextStatus)) {
			return changeStatus(this.issue, nextStatus);
		} else {
			throw new ValidationException("Invalid transition from the status: " + this.issue.getStatus().getName());
		}
	}
	
	/**
	 * Do the operations needed to change the status of an issue
	 * 
	 * @param issue Issue to change the status
	 * @param nextStatus Next status to move forward to
	 * @return
	 */
	private Issue changeStatus(Issue issue, IssueStatus nextStatus) {
		//TODO Implement the service to change between the status
		//This method is just an example regarding of a multithreading
		if (nextStatus.getCode().equals(2)) { //
			try {
				TimeUnit.SECONDS.sleep(15);
			} catch (InterruptedException e) {
				issue.setStatus(nextStatus);
				return issue;
			}
			issue.setStatus(nextStatus);
			return issue;
		} else if (nextStatus.getCode().equals(3)) { //
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				issue.setStatus(nextStatus);
				return issue;
			}
			issue.setStatus(nextStatus);
			return issue;
		} else {
			issue.setStatus(nextStatus);
		}
		return issue;
	}

	/**
	 * Validate if the next status is valid from the old one.
	 * 
	 * @param statusOld Status that the issue is it.
	 * @param nextStatus The next status that the issue must be.
	 * @return
	 */
	private boolean validTransition(IssueStatus statusOld, IssueStatus nextStatus) {
		if (nextStatus != null && statusOld.getTransitions() != null && !statusOld.getTransitions().isEmpty()) {
			for (StatusTransition transition : statusOld.getTransitions()) {
				if (transition.getStatusTo().equals(nextStatus)) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}

}
