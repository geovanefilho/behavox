/**
 * 
 */
package com.behavox.demo.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import com.behavox.demo.dao.IssueStatusDAO;
import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.model.StatusTransition;
import com.behavox.demo.service.IssueStatusService;

/**
 * @author geovanefilho
 *
 *         Implementation for the services of statuses of an inssue
 */
@Service(value = "issueStatusService")
public class IssueStatusServiceImpl extends GenericServiceImpl<IssueStatus> implements IssueStatusService {

	@Autowired
	private IssueStatusDAO issueStatusDao;
	
	@Autowired
	public IssueStatusServiceImpl(IssueStatusDAO issueStatusDao, @Qualifier("issueStatusValidator") Validator validator) {
    	this.genericDao = issueStatusDao;
    	this.issueStatusDao = issueStatusDao;
    	this.validator = validator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IssueStatus save(IssueStatus issueStatus) {
		setFromInTransictions(issueStatus);

		validateDefault(issueStatus);

		return super.save(issueStatus);
	}

	/**
	 * Validate if has a default status already saved and set default if there
	 * isn't.
	 * 
	 * @param issueStatus
	 */
	private void validateDefault(IssueStatus issueStatus) {
		IssueStatus stDefault = getDefault();
		if (stDefault == null) { // Required to be always one default status
			issueStatus.setStatusDefault(Boolean.TRUE);
		} else if (issueStatus.getStatusDefault()) {
			if (!stDefault.getId().equals(issueStatus.getId())) {
				stDefault.setStatusDefault(Boolean.FALSE);
				merge(stDefault);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IssueStatus getDefault() {
		return this.issueStatusDao.getDefault();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IssueStatus merge(IssueStatus issueStatus) {
		setFromInTransictions(issueStatus);
		validateDefault(issueStatus);

		return super.merge(issueStatus);
	}

	/**
	 * Set status origin in the transitions of a status
	 * 
	 * @param issueStatus
	 */
	private void setFromInTransictions(IssueStatus issueStatus) {
		if (issueStatus.getTransitions() != null) {
			for (StatusTransition trans : issueStatus.getTransitions()) {
				trans.setStatusFrom(issueStatus);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addNewTransition(IssueStatus status, IssueStatus newTransition) {
		StatusTransition trans = new StatusTransition(status, newTransition);

		if (status.getTransitions() == null) {
			status.setTransitions(new ArrayList<StatusTransition>());
		}

		status.getTransitions().add(trans);
		this.merge(status);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addNewTransition(Long idFrom, Long idTo) {
		IssueStatus from = this.issueStatusDao.getOne(idFrom);
		IssueStatus to = this.issueStatusDao.getOne(idTo);

		this.addNewTransition(from, to);
	}

}
