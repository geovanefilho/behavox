package com.behavox.demo.service.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.behavox.demo.model.User;

/**
 * Class responsible for validate an user
 * 
 * @author geovanefilho
 *
 */
@Component("userValidator")
public class UserValidator implements Validator {

	private int MAX_NAME_LENGTH = 150; //it should use internationalization instead of plain text
	private String NAME_LENGTH_VALIDATION = "must have up to " + MAX_NAME_LENGTH + " caracters.";
	private int MAX_USERNAME_LENGTH = 150;
	private String USERNAME_LENGTH_VALIDATION = "must have up to " + MAX_USERNAME_LENGTH + " caracters.";
	
	/**
	 * {@inheritDoc}
	 */
    @Override
	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void validate(Object target, Errors errors) {
        
        ValidationUtils.rejectIfEmpty(errors, "name", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD); 
        ValidationUtils.rejectIfEmpty(errors, "username", UtilValidator.REQUIRED_FIELD_VALIDATION_CODE, UtilValidator.REQUIRED_FIELD);
               
        User user = (User) target;
        
        if (user.getName() != null && user.getName().length() > MAX_NAME_LENGTH) {
        	errors.rejectValue("name", UtilValidator.FIELD_VALIDATION_CODE, NAME_LENGTH_VALIDATION);
        }
        
        if (user.getUsername() != null && user.getUsername().length() > MAX_USERNAME_LENGTH) {
        	errors.rejectValue("name", UtilValidator.FIELD_VALIDATION_CODE, USERNAME_LENGTH_VALIDATION);
        }
	}
}

