/**
 * 
 */
package com.behavox.demo.service.impl;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import com.behavox.demo.dao.IssueDAO;
import com.behavox.demo.model.Issue;
import com.behavox.demo.model.IssueStatus;
import com.behavox.demo.service.IssueService;
import com.behavox.demo.service.IssueStatusService;

/**
 * @author geovanefilho
 *
 *	Implementation for the services of issues
 */
@Service(value="issueService")
public class IssueServiceImpl extends GenericServiceImpl<Issue> implements IssueService {
	
	@Autowired
	private IssueDAO issueDao;
	
	@Autowired
	private IssueStatusService issueStatusService;
	
	private static ExecutorService executor;
	
	@Autowired
	public IssueServiceImpl(IssueDAO issueDao, @Qualifier("issueValidator") Validator validator) {
    	this.genericDao = issueDao;
    	this.issueDao = issueDao;
    	this.validator = validator;
    	
    	IssueServiceImpl.executor = Executors.newCachedThreadPool();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Issue save(Issue issue) {
		issue.setStatus(this.issueStatusService.getDefault());
		
		return super.save(issue);
	}

	/**
	 * {@inheritDoc}
	 */
	public Issue advance(Long idIssue, Long nextStatusId) {
		Issue issue = this.find(idIssue);
		IssueStatus nextStatus = this.issueStatusService.find(nextStatusId);
		
		return this.advance(issue, nextStatus);
	}

	/**
	 * {@inheritDoc}
	 */
	public Issue advance(Issue issue, IssueStatus next) {
		Future<Issue> task = executor.submit(new ChangeStatusIssue(issue, next));
		try {
			Issue issueChanged = task.get();
			return this.issueDao.save(issueChanged);
		} catch (InterruptedException | ExecutionException e) {
			throw new ValidationException("It was not possible to change the status due to: " + e.getMessage());
		}
	}

}
