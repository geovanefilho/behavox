/**
 * 
 */
package com.behavox.demo.service;

import org.springframework.beans.factory.annotation.Qualifier;

import com.behavox.demo.model.IssueStatus;

/**
 * @author geovanefilho
 *
 *	Services available for statuses of an issue
 */
@Qualifier(value = "issueStatusService")
public interface IssueStatusService extends GenericService<IssueStatus> {
	
	/**
	 * Create a new transition for the status
	 * 
	 * @param status Status to create a new transition
	 * @param newTransition The new status that the previews status can transit to
	 */
	public void addNewTransition(IssueStatus status, IssueStatus newTransition);

	/**
	 * Create a new transition for the status
	 * 
	 * @param status Status to create a new transition
	 * @param newTransition The new status that the previews status can transit to
	 */
	public void addNewTransition(Long idFrom, Long idTo);

	/**
	 * Get the status default
	 * 
	 * @return
	 */
	IssueStatus getDefault();
}
